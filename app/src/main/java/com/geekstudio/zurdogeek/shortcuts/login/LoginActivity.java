package com.geekstudio.zurdogeek.shortcuts.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.geekstudio.zurdogeek.shortcuts.MainActivity;
import com.geekstudio.zurdogeek.shortcuts.R;

public class LoginActivity extends AppCompatActivity {

    private EditText edtUser;
    private EditText edtPassword;
    private Button  btnSignin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtPassword = findViewById(R.id.edt_password);
        edtUser = findViewById(R.id.edt_user);
        btnSignin = findViewById(R.id.btn_sign_in);



        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( getApplicationContext(), MainActivity.class));
            }
        });
    }
}
